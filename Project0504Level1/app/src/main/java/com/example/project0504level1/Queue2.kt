package com.example.project0504level1

class Queue2<T> {
    var elements: MutableList<T> = mutableListOf()

    fun enqueue(item: T) = elements.add(item)
    fun dequeue(): T? = if (elements.isNotEmpty()) elements.removeAt(0) else null

    fun sortFun(prediction: (T) -> Boolean): Queue2<T> {
        val queue2 = Queue2<T>()

        for (el in elements) {
            if (prediction(el)) {
                queue2.enqueue(el)
            }
        }
        return queue2
    }

}
fun main() {
    val elements: MutableList<Int> = mutableListOf(1,3,44,5,4,2,3,6,77,24,5)
    val el = Queue2<Int>()

    for (num in elements) {
        el.enqueue(num)
    }

    fun<T: Number> filtration(element: T): Boolean {
        return element.toLong() % 2 == 0L
    }

    val queue2 = el.sortFun(::filtration)
    println(queue2.elements)
}


