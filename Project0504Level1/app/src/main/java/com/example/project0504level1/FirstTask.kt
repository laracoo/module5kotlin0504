package com.example.project0504level1

import kotlin.math.round

fun main(){
    val listOfInputs1: MutableList<Int> = mutableListOf(1,3,5,6,4,3,55,3,22)
    val listOfInputs2: MutableList<Float> = mutableListOf(1f,3f,5f,6.3f,6f,4f,3f,55f,3f,22f)
    val listOfInputs3: MutableList<Double> = mutableListOf(1.0,3.0,5.0,22.2,6.0,4.0,3.0,55.0,3.0,22.0)
    val listOfInputs4: MutableList<Long> = mutableListOf(157484033455,Long.MAX_VALUE, Long.MIN_VALUE,666666662)

    fun <T: Number> genericFunction(listOfInputs: MutableList<T>): List<T> {
        val newListOfInputs: MutableList<T> = mutableListOf()
        for(i in 0 until listOfInputs.size) {
           if(round(listOfInputs[i].toDouble()) == listOfInputs[i] || round(listOfInputs[i].toFloat()) == listOfInputs[i] || listOfInputs[i].toInt() == listOfInputs[i] || listOfInputs[i].toLong() == listOfInputs[i] || listOfInputs[i].toShort() == listOfInputs[i]) {
               newListOfInputs.add(listOfInputs[i])
              }
           }
        return  newListOfInputs.filter { it.toLong() % 2 == 0L}
    }
    println("First List : ${genericFunction(listOfInputs1)}")
    println("Second List : ${genericFunction(listOfInputs2)}")
    println("Third List : ${genericFunction(listOfInputs3)}")
    println("Forth List : ${genericFunction(listOfInputs4)}")
}



